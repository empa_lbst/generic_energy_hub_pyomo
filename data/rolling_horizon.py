# -*- coding: utf-8 -*-
"""
Created on Tue Sep 15 11:55:23 2015

Rolling horizon environment
period equivalent to model time resolution


@author: maju
"""

#-----------------------------------------------------------------------------#
# Imports
#-----------------------------------------------------------------------------#
from pyomo import *
from pyomo.core import *
import pyomo.environ

#-----------------------------------------------------------------------------#
# Rolling horizon parameters
#-----------------------------------------------------------------------------#

class Rolling_horizon(AbstractModel):
    '''
    Set up rolling horizon environment
    '''    
    def __init__(self, model):
        m = model
        
        m.numberOfPeriods = Param(Initialize=m.hours, within=NonNegativeIntegers)
        m.Horizon = RangeSet(m.firstPeriodInPlanningInterval, m.lastPeriodInPlanningInterval, m.Time)

    def not_finished(self, model):
        
        return(- 1 + ord(m.firstPeriodInPlanningInterval) + m.numberOfPeriodsInPlanningInterval < m.numberOfPeriods)        
    
    def initialize_horizon(self, model):
        m = model        
        
        m.numberOfPeriodsInPlanningInterval = m.numberOfPeriodsInPlanningInterval
        m.firstHourInPlanningInterval = Param(Initialize=m.Time.first(), within=NonNegativeIntegers)
        m.firstPeriodInPlanningInterval = Param(Initialize=m.Horizon.first(), within=NonNegativeIntegers)
        create_hour_periods_time_table(model)
        aggregate_input(model)
    
    def roll_horizon_once(self, model):
        m = model
        
        if (-1 + ord(m.firstPeriodInPlanningInterval) + m.numberOfPeriodsInPlanningInterval + m.stepSize ) > m.numberOfPeriods:
            m.numberOfPeriodsInPlanningInterval -= -1 + ord(m.firstPeriodInPlanningInterval) + ( 
                                                    (m.numberOfPeriodsInPlanningInterval + m.stepSize - m.numberOfPeriods))
        m.firstHourInPlanningInterval += m.stepSize
        m.firstPeriodInPlanningInterval += m.stepSize
        
        create_hour_periods_time_table(model)
        aggregate_input(model)
        
    def create_hour_periods_time_table(self, model):
        '''
        time table for receding horizon 
        '''
        m = model
        
#       m.hoursPeriodsTimeTable = Conversion / equivalence periods <=> hours for receding horizon
        m.currentTimeSlot = m.firstHourInPlanningInterval
        m.currentPeriod = m.firstPeriodInPlanningInterval
#        PeriodLength : PeriodLength
#        LengthDominates : LengthDominates
#        InactiveTimeSlots: InactiveHours
#        DelimiterSlots: NewPeriodsStarts
        
    def aggregate_input(self, model):
        '''
        aggregate data over all horizon
        '''
        m = model
        
                
    def run_rolling_horizon(self, model):
        m = model
        
        initialize_horizon(model)
        solve
        while not_finished(model):
            roll_horizon_once(model)
            solve
        print(results)
             
        
    def check_horizon_solution(self, model):
        '''
        check results
        '''

    
