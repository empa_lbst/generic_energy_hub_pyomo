# -*- coding: utf-8 -*-
"""
Created on Tue Sep 15 11:55:23 2015

Rolling horizon environment


@author: maju
"""

#-----------------------------------------------------------------------------#
# Imports
#-----------------------------------------------------------------------------#
from pyomo.core import *
import pyomo.environ

#-----------------------------------------------------------------------------#
# Rolling horizon parameters
#-----------------------------------------------------------------------------#



def initialize_horizon:

def roll_horizon_once:
    
def create_hour_periods_time_table:
    
def aggregate_input:
    
def run_rolling_horizon:
    
def check_horizon_solution:
    
    
