# -*- coding: utf-8 -*-
"""
Created on Wed Aug  5 10:35:25 2015

Copyright (C) : info to add

Generic_energy_hub_model.py
~~~~~~~~~~~~~

Simple energy hub model: 1 energy hub:

*Technologies:
    - Heat Pump HP
    - Boiler
    - CHP
    - PV
    - ST
*Storages:
    - Battery
    - Hot water tank

* 1 building (heat, electricity demand)

@author: Julien 
"""

#-----------------------------------------------------------------------------#
# Imports
#-----------------------------------------------------------------------------#
from pyomo.core import *
from pyomo.opt import SolverFactory, SolverManagerFactory
import pyomo.environ

import xlrd
import numpy as np
import time
start_time = time.time()

#-----------------------------------------------------------------------------#
## read excel demand profile data
#-----------------------------------------------------------------------------#
path_demand = 'C:/Users/maju/Desktop/Energy_hub_Python_PYOMO/data/Demand.xls'
path_solar = 'C:/Users/maju/Desktop/Energy_hub_Python_PYOMO/data/Data.xls'
# demand book
book = xlrd.open_workbook(path_demand)
# get the first worksheet
first_sheet = book.sheet_by_index(0) 
# read a column slice
elecload_init_1 = [cell.value for cell in first_sheet.col_slice(colx=0,start_rowx=0,end_rowx=8760)]
heatload_init_1 = [cell.value for cell in first_sheet.col_slice(colx=1, start_rowx=0, end_rowx=8760)]
# solar profile
book = xlrd.open_workbook(path_solar)
# get the first worksheet
first_sheet = book.sheet_by_index(0) 
# read a column slice
solar_init_1 = [cell.value for cell in first_sheet.col_slice(colx=0,start_rowx=0,end_rowx=8760)]

elecload_init={}
for i in range(1, len(elecload_init_1)+1):  #len(elecload_init_1)+1
    elecload_init[i]=elecload_init_1[i-1]
    
heatload_init={}
for i in range(1, len(heatload_init_1)+1): #len(heatload_init_1)+1
    heatload_init[i]=heatload_init_1[i-1]
    
solar_init={}
for i in range(1, len(solar_init_1)+1): #len(heatload_init_1)+1
    solar_init[i]=solar_init_1[i-1]/1000 # W -> kW
    
loads_init={}
for i in range(1, len(elecload_init)+1):
    loads_init[i,1] = elecload_init[i]
    loads_init[i,2] = heatload_init[i]


loads_init=np.zeros((len(elecload_init),2))
for i in range(1, len(elecload_init)+1):
    loads_init[i-1,0] = elecload_init[i]
    loads_init[i-1,1] = heatload_init[i]
    
def array_to_dict(array):
    if len(array.shape) == 2:
        return {(i+1,j+1):np.asscalar(array[i,j]) for i in range(array.shape[0]) for j in range(array.shape[1])}
    else:
        raise NotImplemented
        
loads_dict=array_to_dict(loads_init)

#-----------------------------------------------------------------------------#
## Creating a model ##
#-----------------------------------------------------------------------------#
model = AbstractModel()
# number of hours, technologies, timesteps
model.hours = Param(within=NonNegativeIntegers)
model.technologies = Param(within=NonNegativeIntegers)
model.demand = Param(within=NonNegativeIntegers)
# sets definition
model.Time = RangeSet(1, model.hours)
model.SubTime = RangeSet(2, model.hours, within=model.Time)
model.In = RangeSet(1, model.technologies)
model.W_Ogrid = Set(initialize=[2,3,4,5,6,7,8], within=model.In)
model.Out = RangeSet(1, model.demand)
model.SolarTechs = Set(initialize=[5, 6], within=model.In)
model.DispTechs = Set(initialize=[2,3,4,7,8], within=model.In)
model.CHP = Set(initialize=[4,7,8], within=model.In)

# coupling matrix & Technical parameters
model.cMatrix = Param(model.In, model.Out)                                      # coupling matrix technology efficiencies 
model.maxCapTechs = Param(model.DispTechs)
model.maxStorage = Param(initialize=100)
model.maxStorCh = Param(model.Out)
model.maxStorDisch = Param(model.Out)
model.lossesStorStanding = Param(model.Out)
model.chargingEff = Param(model.Out)
model.dischLosses = Param(model.Out)
model.minSoC = Param(model.Out)
model.partLoad = Param(model.In, model.Out)
model.maxSolarArea = Param(initialize=50)

# carbon factors
model.carbonFactors = Param(model.In)
model.maxCarbon = Param(initialize=1000000)

# Cost parameters
model.linCapCosts = Param(model.In, model.Out)                                  # Technologies capital costs
model.fixCapCosts = Param(model.In, model.Out)
model.linStorageCosts = Param(model.Out)
model.opPrices = Param(model.In)                                                # Operating prices technologies
model.feedInTariffs = Param(model.Out)                                          # feed-in tariffs
model.omvCosts = Param(model.In)                                                # Maintenance costs
model.interestRate = Param(within=NonNegativeReals)
# lifetime
model.lifeTimeTechs = Param(model.In)
model.lifeTimeStorages = Param(model.Out)

## Declaring Global Parameters ##
model.timeHorizon = Param(within=NonNegativeReals)
model.bigM = Param(within=NonNegativeReals)

#loads
model.elecLoad = Param(model.Time, initialize=elecload_init)
model.heatLoad = Param(model.Time, initialize=heatload_init)
model.loads = Param(model.Time, model.Out, initialize=loads_dict)
model.solarEm = Param(model.Time, initialize=solar_init)
#model.elecLoad = Param(model.Time)
#model.heatLoad = Param(model.Time)


# Relax domain of Var(capacities) from integers to reals
def relaxCapacitiesVar_rule(model, inp, out):    
    if inp == 2 or inp == 3 or inp == 4 or inp == 7 or inp == 8:
        return(NonNegativeReals)
    else:
        return(NonNegativeIntegers)

## Global variables
model.P = Var(model.Time, model.In, domain=NonNegativeReals)
model.Pexport = Var(model.Time, model.Out, domain=NonNegativeReals)
model.Capacities = Var(model.In, model.Out, domain=relaxCapacitiesVar_rule)
model.Ytechnologies = Var(model.In, model.Out, domain=Binary)
model.Yon = Var(model.Time, model.In, domain=Binary)
model.TotalCost = Var(domain=Reals)
model.OpCost = Var(domain=NonNegativeReals)
model.MaintCost = Var(domain=NonNegativeReals)
model.IncomeExp = Var(domain=NonNegativeReals)
model.InvCost = Var(domain=NonNegativeReals)
model.TotalCarbon = Var(domain=Reals)
model.NetPresentValueTech = Var(model.In, domain=NonNegativeReals)
model.NetPresentValueStor = Var(model.Out, domain=NonNegativeReals)

#Storage variables
model.Qin = Var(model.Time, model.Out, domain=NonNegativeReals)
model.Qout = Var(model.Time, model.Out, domain=NonNegativeReals)
model.E = Var(model.Time, model.Out, domain=NonNegativeReals)
model.StorageCap = Var(model.Out)
model.Ystorage = Var(model.Out, domain=Binary)


#-----------------------------------------------------------------------------#
## GLobal constraints
#-----------------------------------------------------------------------------#
def loadsBalance_rule(model, t, out):
    return (model.loads[t,out] + model.Pexport[t,out] <= (model.Qout[t,out] - model.Qin[t,out] + 
                                                        sum(model.P[t,inp]*model.cMatrix[inp,out] for inp in model.In)))
model.loadsBalance = Constraint(model.Time, model.Out, rule=loadsBalance_rule)

def capacityConst_rule(model, t, inp, out):
    if model.cMatrix[inp,out] <= 0:
        return(Constraint.Skip)
    else:
        return (model.P[t, inp] * model.cMatrix[inp,out] <= model.Capacities[inp,out])
model.capacityConst = Constraint(model.Time, model.In, model.Out, rule=capacityConst_rule)

def maxCapacity_rule(model, disp, out):
    if disp==4 or disp==7 or disp==8 or model.cMatrix[disp,out] == 0:
        return(Constraint.Skip)
    else:
        return (model.Capacities[disp, out] <= model.maxCapTechs[disp])
model.maxCapacity = Constraint(model.DispTechs, model.Out, rule=maxCapacity_rule)

def capacity_rule(model, inp, out):
    if model.cMatrix[inp,out] <= 0:
        return(model.Capacities[inp,out] == 0)
    else:
        return(Constraint.Skip)
model.capacity_feasibility = Constraint(model.In, model.Out, rule=capacity_rule)

#def maxCapacityBIS_rule(model):
#    return (model.Capacities[2, 2] == 10)
#model.maxCapacityBIS = Constraint(rule=maxCapacityBIS_rule)

def carbonConst_rule(model):
    return (model.TotalCarbon <= model.maxCarbon)
model.carbonConst = Constraint(rule=carbonConst_rule)

#-----------------------------------------------------------------------------#
## Specific constraints 
#-----------------------------------------------------------------------------#
def partLoadL_rule(model, t, disp, out):  
    if model.cMatrix[disp,out] <= 0:
        return(Constraint.Skip)
    else:
        return (model.partLoad[disp, out] * model.Capacities[disp, out] <= 
                                                                    (model.P[disp, out] * model.cMatrix[disp,out] 
                                                                    + model.bigM * (1 - model.Yon[t, disp])))
model.partLoadL = Constraint(model.Time, model.DispTechs, model.Out, rule=partLoadL_rule)

def partLoadU_rule(model, t, disp, out):    
    if model.cMatrix[disp,out] <= 0:
        return(Constraint.Skip)
    else:
        return (model.P[t, disp] * model.cMatrix[disp, out] <= model.bigM * model.Yon[t, disp])
model.partLoadU = Constraint(model.Time, model.DispTechs, model.Out, rule=partLoadU_rule)

def solarInput_rule(model, t, sol, out):
    if model.cMatrix[sol, out] <= 0:
        return(Constraint.Skip)
    else:
        return (model.P[t, sol] == model.solarEm[t] * model.Capacities[sol, out] )
model.solarInput = Constraint(model.Time, model.SolarTechs, model.Out, rule=solarInput_rule) 

def roofArea_rule(model):
    return (model.Capacities[5,1] + model.Capacities[6,2] <= model.maxSolarArea)
model.roofArea = Constraint(rule=roofArea_rule)

def fixCostConst_rule(model, inp, out):
    return (model.Capacities[inp,out] <= model.bigM * model.Ytechnologies[inp,out])
model.fixCostConst = Constraint(model.In, model.Out, rule=fixCostConst_rule)

def h2pRatio_rule(model, chp):
    return (model.Capacities[chp,2] == model.cMatrix[chp,2] / model.cMatrix[chp,1] * model.Capacities[chp,1])
model.h2pRatio = Constraint(model.CHP, rule=h2pRatio_rule)

def chp_rule(model, chp):
    return (model.Ytechnologies[chp,1]==model.Ytechnologies[chp,2])
model.chpConst = Constraint(model.CHP, rule=chp_rule)

def chpDiscrete_rule(model, chp):
    return (model.Capacities[chp,1] == model.maxCapTechs[chp] * model.Ytechnologies[chp,1])
model.chpDiscrete = Constraint(model.CHP, rule=chpDiscrete_rule)

#-----------------------------------------------------------------------------#
## Storage constraints
#-----------------------------------------------------------------------------#
def storageBalance_rule(model, t, out):
    return (model.E[t, out] == (
                                model.lossesStorStanding[out] * model.E[(t-1), out] 
                                + model.chargingEff[out] * model.Qin[t, out] 
                                - (1/model.dischLosses[out]) * model.Qout[t, out]))
model.storageBalance = Constraint(model.SubTime, model.Out, rule=storageBalance_rule)

def storageInitBattery_rule(model):
    return (model.E[1, 1] == model.StorageCap[1] * model.minSoC[1])
model.storageInitBattery = Constraint(rule=storageInitBattery_rule)

def storageInitThermal1_rule(model):
    return (model.E[1, 2] == model.E[8760, 2])
model.storageInitThermal1 = Constraint(rule=storageInitThermal1_rule)

def storageInitThermal2_rule(model):
    return (model.Qout[1, 2] == 0)
model.storageInitThermal2 = Constraint(rule=storageInitThermal2_rule)

def storageChargeRate_rule(model, t, out):
    return (model.Qin[t,out] <= model.maxStorCh[out] * model.StorageCap[out])
model.storageChargeRate = Constraint(model.Time, model.Out, rule=storageChargeRate_rule)

def storageDischRate_rule(model, t, out):
    return (model.Qout[t,out] <= model.maxStorDisch[out] * model.StorageCap[out])
model.storageDischRate = Constraint(model.Time, model.Out, rule=storageDischRate_rule)

def storageMinState_rule(model, t, out):
    return (model.E[t,out] >= model.StorageCap[out] * model.minSoC[out])
model.storageMinState = Constraint(model.Time, model.Out, rule=storageMinState_rule)

def storageCap_rule(model, t, out):
    return (model.E[t,out] <= model.StorageCap[out] )
model.storageCap = Constraint(model.Time, model.Out, rule=storageCap_rule)

def storageMaxCap_rule(model, out):
    return (model.StorageCap[out] <= model.maxStorage )
model.maxConstraintStorage = Constraint(model.Out, rule=storageMaxCap_rule)


#-----------------------------------------------------------------------------#
## Objective functions
#-----------------------------------------------------------------------------#
# Minimization of the total costs: operating and investment costs 
def objective_rule(model):
    return (model.TotalCost)
model.Total_Cost = Objective(rule=objective_rule, sense=minimize)

# Minimization of the total carbon emission (operation)
#def objective_rule(model):
#    return (model.TotalCarbon)
#model.Total_Carbon = Objective(rule=objective_rule, sense=minimize)

# Operating costs
def opCost_rule(model):
    return(model.OpCost == ((sum (model.opPrices[inp] 
                            * sum(model.P[t,inp] for t in model.Time)
                            for inp in model.In)))
                            )
model.opCost = Constraint(rule=opCost_rule) 

# Maintenance costs
def maintenanceCost_rule(model):    
    return(model.MaintCost == (sum(model.P[t,inp] * 
                              model.cMatrix[inp,out] * model.omvCosts[inp] * (model.cMatrix[inp, out] > 0)
                              for t in model.Time for inp in model.In for out in model.Out)
                              ))
model.maintCost = Constraint(rule=maintenanceCost_rule)

# Income from feed-in tariffs
def incomeExp_rule(model):
    return(model.IncomeExp == (sum(model.feedInTariffs[out] * 
                            sum(model.Pexport[t,out] for t in model.Time) 
                            for out in model.Out))
                            )
model.incomeExp = Constraint(rule=incomeExp_rule)

# Investments costs for technologies
def invCost_rule(model):
    return(model.InvCost == (sum(model.NetPresentValueTech[inp] * (model.linCapCosts[inp,out] * model.Capacities[inp,out] + model.fixCapCosts[inp,out] * model.Ytechnologies[inp,out]) for inp in model.W_Ogrid for out in model.Out)
                            + sum(model.NetPresentValueStor[out] * model.linStorageCosts[out] * model.StorageCap[out] for out in model.Out))
                            )
model.invCost = Constraint(rule=invCost_rule) 

# Total cost rule
def totalCost_rule(model):
    return(model.TotalCost == model.InvCost + model.OpCost + model.MaintCost - model.IncomeExp)
model.totalCost = Constraint(rule=totalCost_rule) 

# Carbon cost rule
def totalCarbon_rule(model):
    return(model.TotalCarbon == sum(model.carbonFactors[inp] * sum(model.P[t,inp] for t in model.Time) for inp in model.In))
model.totalCarbon = Constraint(rule=totalCarbon_rule)

# To replace npv by Capital recovery factory (inverse of net present value)
def npvTech_rule(model, inp):
    return (model.NetPresentValueTech[inp] == 1 / (((1 + model.interestRate) ** model.lifeTimeTechs[inp] - 1) / (model.interestRate * ((1 + model.interestRate) ** model.lifeTimeTechs[inp]))))
model.npvTech = Constraint(model.W_Ogrid, rule=npvTech_rule)

def npvStor_rule(model, out):
    return (model.NetPresentValueStor[out] == 1 / (((1 + model.interestRate) ** model.lifeTimeStorages[out] - 1) / (model.interestRate * ((1 + model.interestRate) ** model.lifeTimeStorages[out]))))
model.npvStor = Constraint(model.Out, rule=npvStor_rule)

#-----------------------------------------------------------------------------#
## Print model ##
#-----------------------------------------------------------------------------#
#model.pprint()  # to activate in order to get an overview of the model (listing of constraints, variables, objective)

# create an instance of the model depending on the data in the file 'generic_model.dat'
instance = model.create("generic_model.dat")
#instance.pprint() # to activate in order to get an overview of the model instance

def mycallback(gurobi, model):                                                  # callback routine: access solver parameters while solving
    if where == GRB.Callback.POLLING: 
        print(GRB.Callback.RUNTIME)
    else:
        pass

# select the MILP solver
opt = SolverFactory("gurobi")                                                   
opt.options["mipgap"]=0.001                                                     # define optimality gap
#opt.keepfiles=True                                                              # keep log files in temp folder property 'keepfiles' depreciates 
opt.Tee=True
#opt.allow_callbacks=True
opt.set_callback("trial", mycallback)

solver_manager = SolverManagerFactory("serial")                                 # solve instances in series
results = solver_manager.solve(instance, tee=True, opt=opt, timelimit=None)
#results = opt.solve(instance)
#stream solver to check while solving
#sends results to stdout
#results.write()
instance.load(results)

#Examples of getting results
P_matrix=np.zeros(shape=(instance.hours.value,instance.technologies.value))
for i in range(1,instance.hours.value+1):
    for j in range(1,instance.technologies.value+1):
        P_matrix[i-1,j-1]=instance.P[i,j].value
        
E_matrix=np.zeros(shape=(instance.hours.value,instance.demand.value))
for i in range(1,instance.hours.value+1):
    for j in range(1,instance.demand.value+1):
        E_matrix[i-1,j-1]=instance.E[i,j].value
        
Qout_matrix=np.zeros(shape=(instance.hours.value,instance.demand.value))
for i in range(1,instance.hours.value+1):
    for j in range(1,instance.demand.value+1):
        Qout_matrix[i-1,j-1]=instance.Qout[i,j].value
        

Qin_matrix=np.zeros(shape=(instance.hours.value,instance.demand.value))
for i in range(1,instance.hours.value+1):
    for j in range(1,instance.demand.value+1):
        Qin_matrix[i-1,j-1]=instance.Qin[i,j].value


'''
# Method to save optimisation results to txt file 
def pyomo_save_results(options=None, instance=None, results=None):
    OUTPUT = open('C:/Users/maju/Desktop/Energy_hub_Python_PYOMO/data/Results_generic_hub.txt','w')
    print(results, file=OUTPUT)
    OUTPUT.close()
#pyomo_save_results(instance=instance, results=results)    # to activate in order to save the results in txt file 

#Example of how to print variables
print(instance.TotalCost.value)
#for i in instance.Pnaturalgas:
#    print (instance.Pnaturalgas[i], instance.Pnaturalgas[i].value)

print("--- %s seconds ---" % (time.time() - start_time))   # print execution time

## create the instance
#instance = model.create(’DiseaseEstimation.dat’)
## define the solver and its options
#solver = ’ipopt’
#opt = SolverFactory( solver )
#if opt is None:
#raise ValueError, "Problem constructing solver \
#‘"+str(solver)
#opt.set_options(’max_iter=2’)
## create the solver manager
#solver_manager = SolverManagerFactory( ’serial’ )
## solve
#results = solver_manager.solve(instance, opt=opt, tee=True,\
#timelimit=None)
#instance.load(results)
## display results
#display(instance)
'''