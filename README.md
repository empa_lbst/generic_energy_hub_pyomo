# README #

### General description

This HUES module contains a generic energy hub model built in the Pyomo environment, Python based. In the HUES platform, other implementations of the same model also exist in AIMMS and ILOG CPLEX. The model addresses both design and operational aspects of an energy hub. The problem can be applied either in the case of a single building or an aggregation of buildings for which the energy demands are supplied in their entirety by the energy hub.

### Set-up
* We assume you have Python 2.7 installed (if not, we recommend the Anaconda distribution)
* Install Pyomo (“pip install pyomo”)
* Install a MILP solver (Gurobi or CPlex); the code here is for Gurobi, but can be changed by editing the following line: opt = SolverFactory("gurobi")   

### Usage
The model definitions are contained in the file generic_model.dat

### More information

A more detailed model description is available in the "Documentation_model.pdf".

### Authors

Julien Marquant, Georgios Mavromatidis,  Akomeno Omu, Ralph Evins